import java.io.File;
import java.util.Scanner;

public class Exo2 {                                                             // ls
    public static void main (String [] args){
        Scanner sc = new Scanner(System.in);                                    // Creation d'un scanner
        System.out.println("Veuillez saisir un path :");
        String path = sc.nextLine();                                            // Recuperation de l'entree utilisateur
        System.out.println("Vous avez saisi : " + path);

        File listDir = new File(path);                                          // Recuperation des infos du dossier en question
        File[] filesList = listDir.listFiles();                                 // Recuperation de la liste des fichier de ce dossier
        for(File f : filesList){
            if(f.isDirectory()){                                                // Si c'est un dossier, j'affiche un "/" a la fin
              System.out.println(f.getName() + "/");
            }
            if(f.isFile()){                                                     // Si c'est un fichier, j'affiche que le nom
                System.out.println(f.getName());
            }
        }

        sc.close();                                                             // Fermeture du scanner
    }
}
