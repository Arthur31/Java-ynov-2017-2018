import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.Scanner;


public class Exo6 {
    public static void main(String[] arguments) throws IOException
    {
      Scanner sc = new Scanner(System.in);                                      // Creation d'un scanner
      System.out.println("Enter an URL to download :");
      String urlstr = sc.nextLine();                                            // Recuperation de l'entree utilisateur pour l'url

      System.out.println("Enter an file name for download ( witht .jpg ) :");
      String nameFile = sc.nextLine();                                          // Recuperation de l'entree utilisateur pour le nom du fichier sur le PC


      URL url = new URL(urlstr);                                                // Creation d'un flux web a partir d'une url
      BufferedImage img = ImageIO.read(url);                                    // Recuperation du flux de donnee et stockage dans un buffer
      File file = new File(nameFile);                                           // Creation du fichier de destination
      ImageIO.write(img, "png", file);                                          // Ecriture du buffer dans le fichier suivant l'encodage png
    }
}
