import java.io.*;
import java.util.Scanner;

class Exo4 {                                                                    // nano

    public static void main(String[] args) {

        try {                                                                   // je cree un try catch general car je ne compte pas triller les erreurs une pas une
            Scanner sc = new Scanner(System.in);                                // Creation d'un scanner
            System.out.println("Nom du fichier (.txt) :");
            String nameFile = sc.nextLine();                                    // Recuperation de l'entree utilisateur pour le nom du fichier à ecrire


            FileWriter fw = new FileWriter("./" + nameFile + ".txt");           // Creation d'un flux d'ecriture fichier

            System.out.println("vous editez le fichier : " + "./" + nameFile + ".txt");
            while(true){                                                        // je cré une boucle infinie
                String input = sc.nextLine();                                   // Pour chaque entree utilisateur
                if(input.equalsIgnoreCase("quit")){                             // Si elle est differente de "quit"
                    break;                                                      // je quitre la boucle
                }

                fw.write(input + "\n");                                         // j'ecris la ligne dans le fichier avec un retour chariot

            }
            sc.close();                                                         // Fermeture du scanner
            fw.close();                                                         // Je ferme le flux fichier
        } catch (IOException e) {                                               // Mon programme peut renconter que des IOException donc je catch que celle la
            e.printStackTrace();                                                // j'affiche l'exeption
        }

    }
}
