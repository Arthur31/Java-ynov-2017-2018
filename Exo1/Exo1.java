import java.io.File;
import java.util.Scanner;

public class Exo1 {                                                             // Verifie si un chemin specifie existe ou pas
    public static void main (String [] args){
        Scanner sc = new Scanner(System.in);                                    // Creation d'un scanner
        System.out.println("Veuillez saisir un path :");
        String path = sc.nextLine();                                            // Recuperation de l'entree utilisateur
        System.out.println("Vous avez saisi : " + path);

        File f = new File(path);                                                // Recuperation des infos du dossier / fichier specifié
        if (f.exists()) {                                                       // Verification de l'existance du dossier / fichier
            if (f.isDirectory()){                                               // chech si c'est un dossier
                System.out.println("Dir");
            }else {                                                             // cas ou c'est un fichier
                System.out.println("File");
            }
        }
        else{
            System.out.println("NOT Exists");                                   // Affichage si le path existe pas
        }

        sc.close();                                                             // Fermeture du scanner
    }
}
