import java.io.File;
import java.util.Scanner;

public class Exo3 {
    public static void main (String [] args){                                   // ls | grep *<extention>
        Scanner sc = new Scanner(System.in);                                    // Creation d'un scanner
        System.out.println("Veuillez saisir un path :");
        String path = sc.nextLine();                                            // Recuperation de l'entree utilisateur pour le path
        System.out.println("Veuillez saisir une extention :");
        String userExt = sc.nextLine();                                         // Recuperation de l'entree utilisateur pour l'extention

        File listDir = new File(path);                                          // Recuperation des infos du dossier en question
        File[] filesList = listDir.listFiles();                                 // Recuperation de la liste des fichier de ce dossier
        for(File f : filesList){

            if(f.isFile() && f.getName().substring(f.getName().lastIndexOf('.')).equals(userExt)){   // Si c'est un fichier, je recupere l'extention et je la compare a l'entree utilisateur
                System.out.println(f.getName());                                // si les condition sont verifie ( fichier et extention) j'affiche le nom du fichier
            }
        }
        sc.close();                                                             // Fermeture du scanner

    }
}
