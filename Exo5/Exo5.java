import java.io.*;
import java.util.Scanner;

class Exo5 {

    public static void main(String[] args) {

        try {
          Scanner sc = new Scanner(System.in);                                  // Creation d'un scanner
            System.out.println("Nom du fichier source :");
            String nameFileS = sc.nextLine();                                   // Recuperation de l'entree utilisateur pour le nom du fichier source


            System.out.println("Nom du fichier cible :");
            String nameFileT = sc.nextLine();                                   // Recuperation de l'entree utilisateur pour le nom du fichier cible

            File source = new File(nameFileS);                                  // Recuperation du fichier source
            File target = new File(nameFileT);                                  // creation du fichier cible




            InputStream streamInput = new FileInputStream(source);              // Creation du flux de lecture pour le fichier source
            OutputStream streamOutput = new FileOutputStream(target);           // Creation du flux d'ecriture pour le fichier cible

            byte[] buffer = new byte[1024];                                     // creation d'un buffer de 1024 bytes
            int length;
            while ((length = streamInput.read(buffer)) > 0) {                   // tant qu'on est pas a la fin du fichier
                streamOutput.write(buffer, 0, length);                          // on ransfere le buffer
            }


            sc.close();                                                         // Fermeture du scanner
            streamInput.close();                                                // Fermeture du stream de lecture
            streamOutput.close();                                               // Fermeture du stream d'ecriture

        } catch (Exception e) {                                                 // Mon programme peut renconter que des IOException donc je catch que celle la
            e.printStackTrace();                                                // j'affiche l'exeption
        }

    }
}
